function valid(input, id, name) {
  choose = 'string';
  /* ================== Check For All Input Of Empty Or Not ====================== */
  if(input.value == "") {
    document.getElementById(id).innerHTML = name + "<strong> Can't</strong> Be Empty";
    document.getElementById(id).classList.remove('hide'); 
    document.getElementById(id).classList.add('show'); 
    input.focus();
    checkCode = 0;
  } else {
      document.getElementById(id).classList.add('hide'); 
      document.getElementById(id).classList.remove('show'); 
      checkCode = 1;
      checPass  = 1;
  }
  /* ================== Check For Code Input ====================== */
  if(input.name == "code") {
    if(input.value.length > 0 && input.value.length < 8) {
        document.getElementById(id).innerHTML = "Code<strong> Must</strong> Contain 8 Number";
        document.getElementById(id).classList.remove('hide'); 
        document.getElementById(id).classList.add('show'); 
        checkCode = 0;
      }
      else if(input.value.length == 8) {
          document.getElementById(id).classList.add('hide'); 
          document.getElementById(id).classList.remove('show'); 
          checkCode = 1;
            console.log(checkCode);
      }
  }
  /* ================== Check For Password Input ====================== */
  if(input.name == "pass") {
    if(input.value.length > 0 && input.value.length < 5) {
      document.getElementById(id).innerHTML = "Password<strong> Must</strong> Larger Than 5 Number";
      document.getElementById(id).classList.add('show');  
      document.getElementById(id).classList.remove('hide');
      checPass = 0; 
    } else {
      document.getElementById(id).classList.add('hide');   
      checPass = 1;   
    }
  }

}