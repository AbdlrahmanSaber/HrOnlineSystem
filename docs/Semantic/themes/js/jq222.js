/* Start: navbar Style (fixed) */
$(document).ready(function() {

	$(".dropdown").click(function() {
		$(this).find(".dropdown-menu").fadeToggle(200);
        $(this).siblings(".dropdown").find(".dropdown-menu").fadeOut(100);
	})


/* ==================== Section Cutomer ========================= */
	/* Start: OWL CAROUSEL(customer reviews) */
	$('#reviews .owl-carousel').owlCarousel({
        items:3,
        merge:true,
        loop:true,
        margin:10,
        center:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:3
            }
        }
	});
	/* End: OWL CAROUSEL(customer reviews) */

	/* Start: OWL CAROUSEL(customers) */
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		responsiveClass: true,
		responsive: {
		  0: {
		    items: 1
		  },
		  600: {
		    items: 3
		  },
		  1000: {
		    items: 5,
		    loop: false,
		    margin: 20
		  }
		}
	});
    /* End: OWL CAROUSEL(customers) */
        /* =================== Navbar ================= */
    $(".navbar .navbar-toggle").click(function(){
    	$(".navbar .navbar-collapse").slideToggle(500)
    });

    /*=============== Notificate Tricks Hide ===============*/
    $(".noti").click(function() {
        $(".load").load("read-emp.php", {
        });
        $(this).find(".num").css("display", "none");
    });


    // toggl between color option box
    $(".option-box .gear-check").click(function() {
    	$(".option-box .color-option ").fadeToggle(500);
    })

	//change theme color for list
	var colorLi = $(".option-box .color-option ul li");
	colorLi 
	.eq(0).css("backgroundColor" , "#bd1818" ).end()
	.eq(1).css("backgroundColor" , "#0e118a" ).end()
	.eq(2).css("backgroundColor" , "#FF9899" ).end()
	.eq(3).css("backgroundColor" , "#39bace" ).end()
	.eq(4).css("backgroundColor" , "#076a7a" ).end()
	.eq(5).css("backgroundColor" , "#d1083d" ).end()

	colorLi.click(function(){
		$("link[href*='figure']").attr("href" , $(this).attr("data-value"));
	});

} )
$(window).scroll(function() {
    		  if ($(document).scrollTop() > 60)
		  {
		    $('nav').addClass('navbar-fixed')
		  }
		  else
		  {
		    $('nav').removeClass('navbar-fixed');
		  }
})
/* End: navbar Style (fixed) */