-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2018 at 01:59 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `employeesinfo`
--

CREATE TABLE `employeesinfo` (
  `code` int(11) NOT NULL,
  `f_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) DEFAULT NULL,
  `faculty` varchar(255) DEFAULT NULL,
  `departement` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `password` int(11) DEFAULT NULL,
  `finger_print` int(11) DEFAULT NULL,
  `arrival_time` time DEFAULT NULL,
  `departure_time` time DEFAULT NULL,
  `face_print` int(11) DEFAULT NULL,
  `RF_card_NUM` int(11) DEFAULT NULL,
  `Hiring_date` date DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `personal_email` varchar(255) DEFAULT NULL,
  `organizational_email` varchar(255) DEFAULT NULL,
  `GroupId` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employeesinfo`
--

INSERT INTO `employeesinfo` (`code`, `f_name`, `l_name`, `faculty`, `departement`, `title`, `nationality`, `gender`, `password`, `finger_print`, `arrival_time`, `departure_time`, `face_print`, `RF_card_NUM`, `Hiring_date`, `birthdate`, `mobile`, `Address`, `personal_email`, `organizational_email`, `GroupId`, `avatar`) VALUES
(20152013, 'Abdlrahman', 'Saber', 'medecine', 'cs', 'doctor', 'Egyption', 'male', 123456, NULL, '23:00:00', '24:00:00', 2, 123232, '2018-04-10', '2018-04-13', 1202262457, '29 Al-Akhshid Street in Manil Al-Rawdah', 'abd@gmail.com', 'o@orgainzaional.com', 0, ''),
(20152014, 'abdo', 'saber', 'thebhes', 'cs', 'doctor', 'Egyption', 'male', 123456, NULL, '08:00:00', '19:00:00', 0, 12222, '2018-04-19', '2018-04-06', 1202262457, '29 Al-Akhshid Street in Manil Al-Rawdah', 'abdo@gmail.com', 'abdoW@gmail.com', 0, ''),
(20152017, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, 123456, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(20152022, 'Salma', 'Ahmad', 'thebes', 'cs', 'doctor', 'Egyption', 'female', 123456, NULL, '08:00:00', '23:00:00', 0, 0, '2018-04-18', '2018-04-18', 1202262457, '29 a eldokki streed', 'salma@gmail.com', 'abdo@org.com', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `employeesp`
--

CREATE TABLE `employeesp` (
  `code` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `Departure_time` date DEFAULT NULL,
  `Arrival_time` date DEFAULT NULL,
  `current_count_of_permissions` int(11) DEFAULT NULL,
  `Total_counts_of_permissions` int(11) DEFAULT NULL,
  `Permission_Status` int(11) DEFAULT NULL,
  `PStatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employeessalary`
--

CREATE TABLE `employeessalary` (
  `Code` int(11) DEFAULT NULL,
  `No_of_permission` int(11) DEFAULT NULL,
  `No_of_vacation` int(11) DEFAULT NULL,
  `No_of_arriving_late_in_mins_per_day` int(11) DEFAULT NULL,
  `Slicing_of_late_arrival` int(11) DEFAULT NULL,
  `gross_salary` int(11) DEFAULT NULL,
  `cut_off_in_pounds` int(11) DEFAULT NULL,
  `taxes` int(11) DEFAULT NULL,
  `net_salary` int(11) DEFAULT NULL,
  `payment_method` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employeessalary`
--

INSERT INTO `employeessalary` (`Code`, `No_of_permission`, `No_of_vacation`, `No_of_arriving_late_in_mins_per_day`, `Slicing_of_late_arrival`, `gross_salary`, `cut_off_in_pounds`, `taxes`, `net_salary`, `payment_method`) VALUES
(20152013, 2, 3, 3, 21, 20000, NULL, 10, 16050, 'Male'),
(20152014, 2, 21, 0, 2222, 20000, NULL, 10, 18000, 'Male'),
(20152022, 2, 21, 2, 3, 25000, NULL, 10, 22500, 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `employeesv`
--

CREATE TABLE `employeesv` (
  `code` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `no_of_days` int(11) DEFAULT NULL,
  `Start_Date` int(11) DEFAULT NULL,
  `End_Date` int(11) DEFAULT NULL,
  `Total_days_of_vacation` int(11) DEFAULT NULL,
  `Vacation_status` int(11) DEFAULT NULL,
  `employee_signature` varchar(255) NOT NULL,
  `manager_name` varchar(255) NOT NULL,
  `manager_signature` varchar(255) NOT NULL,
  `VStatus` int(11) NOT NULL DEFAULT '0',
  `Reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employeesv`
--

INSERT INTO `employeesv` (`code`, `type`, `no_of_days`, `Start_Date`, `End_Date`, `Total_days_of_vacation`, `Vacation_status`, `employee_signature`, `manager_name`, `manager_signature`, `VStatus`, `Reason`) VALUES
(20152013, 0, 2, 2147483647, 2147483647, 3, NULL, 'a', 'g', 'f', 1, 'I\'m Tired'),
(20152022, 0, 2, 2147483647, 2147483647, 2, NULL, 'a', 'v', 'x', 1, 'a'),
(20152022, 0, 2, 2147483647, 2147483647, 2, NULL, 'a', 'b', 'a', 1, 'aa');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `usercode` int(11) NOT NULL,
  `admincode` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Nstatus` int(11) NOT NULL DEFAULT '0',
  `accept` varchar(255) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `readStatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`usercode`, `admincode`, `description`, `Title`, `Name`, `Nstatus`, `accept`, `type`, `readStatus`) VALUES
(20152013, 0, 'I\'m Tired', 'Request Vacation', 'Abdlrahman', 1, 'Accept', 'v', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employeesinfo`
--
ALTER TABLE `employeesinfo`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `employeesp`
--
ALTER TABLE `employeesp`
  ADD KEY `code` (`code`);

--
-- Indexes for table `employeessalary`
--
ALTER TABLE `employeessalary`
  ADD KEY `Code` (`Code`);

--
-- Indexes for table `employeesv`
--
ALTER TABLE `employeesv`
  ADD KEY `code` (`code`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD KEY `usercode` (`usercode`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employeesp`
--
ALTER TABLE `employeesp`
  ADD CONSTRAINT `employeesp_ibfk_1` FOREIGN KEY (`code`) REFERENCES `employeesinfo` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employeessalary`
--
ALTER TABLE `employeessalary`
  ADD CONSTRAINT `employeessalary_ibfk_1` FOREIGN KEY (`Code`) REFERENCES `employeesinfo` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employeesv`
--
ALTER TABLE `employeesv`
  ADD CONSTRAINT `employeesv_ibfk_1` FOREIGN KEY (`code`) REFERENCES `employeesinfo` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`usercode`) REFERENCES `employeesinfo` (`code`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
